﻿﻿# Finding induced subgraphs

This program finds all induced subgraphs in a graph by the given number of vertices and edges. The graph itself is stored in a created data structure with linked arrays for the edges and vertices.

## Getting Started

Program is written in Visual Studio. To see and edit the project just clone the repository files and run the solution file.

## Usage

The graphs in the program are saved in a direct link array. So far the only way to input the original graph is by inserting the array itself in the code and using it in the main method. 

In the UI the button "Generuoti grafą" will display the original graph with its vertices and edges. The user can insert the number of vertices k and the number of edges m of the induced subgraph at the bottom of the UI. After pressing the "Generuoti indukuotuosius pografius" button all the induced subgraphs with the given number of vertices and edges will be displayed. Also the number of induced subgraphs with the given number of vertices will be displayed at the top near the original graph.
