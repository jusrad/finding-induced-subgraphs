﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Code;

// 6. Duotas n viršūninis grafas.Sudaryti algoritmą ir programą visų jo k-viršūninių (k<n) indukuotų 
// pografių, turinčių m briaunų, generavimui.

namespace WindowsFormsApp1
{
    static class Program
    {
        public const int MaxNodes = 10;
        public const int EllipseSize = 20;
        public const int LineEndPush = EllipseSize / 2;
        public const int NumberPositionPush = 4;
        public const int NumberFontSize = 10;

        public const int RofCircle = 50;
        public const int NodeXPush = 125;
        public const int NodeYPush = 65;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
