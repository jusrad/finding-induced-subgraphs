﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Code
{
    public class Edge
    {
        public int startNumber;
        public int endNumber;

        public double StartX;
        public double StartY;
        public double EndX;
        public double EndY;

        public Edge(int startNumber, int endNumber, double StartX, double StartY, double EndX, double EndY)
        {
            this.startNumber = startNumber;
            this.endNumber = endNumber;
            this.StartX = StartX;
            this.StartY = StartY;
            this.EndX = EndX;
            this.EndY = EndY;
        }
    }
}
