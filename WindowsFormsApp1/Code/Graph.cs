﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Code
{
    public class Graph
    {
        public Node[] GraphNodes;
        int NodeCount;
        int ConnectionCount;

        List<Node> NodesToRemove;

        public Graph(Node[] nodes, int Ccount)
        {
            NodesToRemove = new List<Node>();
            this.GraphNodes = new Node[nodes.Length];
            this.GraphNodes = nodes;
            NodeCount = nodes.Length;
            ConnectionCount = Ccount;
            GeneratingCoordinates();
            FindsNodeConnections();
        }

        public Graph(Node[] nodes)
        {
            NodesToRemove = new List<Node>();
            this.GraphNodes = new Node[nodes.Length];
            this.GraphNodes = nodes;
            NodeCount = nodes.Length;
        }

        /// <summary>
        /// Randa viršūnių jungtis pagal vidinį objekto masyvą
        /// </summary>
        public void FindsNodeConnections()
        {
            for (int i = 0; i < GraphNodes.Length; i++)
            {
                for(int j = 0; j < GraphNodes.Length; j++)
                {
                    GraphNodes[i].CheckIfNodesAreConnected(GraphNodes[j]);
                }
            }
        }

        /// <summary>
        /// Pašalinta visas rastas viršūnes
        /// </summary>
        public void RemovesNodeFromArray()
        {
            foreach(Node node in NodesToRemove)
            {
                for (int i = 0; i < GraphNodes.Length; i++)
                {
                    var numberList = GraphNodes[i].NumbersOfConnectedNodes.ToList();
                    numberList.Remove(node.NodesNumber);
                }
            }
        }

        /// <summary>
        /// Iš orginalaus grafo viršūnių masyvo randa viršūnes, kurių nėra pografyje ir kurias reiks pašalinti
        /// </summary>
        /// <param name="original">Orginalaus grafo viršūnių masyvas</param>
        public void FindNodesToRemove(Node[] original)
        {
            List<Node> remove = new List<Node>();
            for (int i = 0; i < original.Length; i++)
            {
                int j;
                for(j = 0; j < GraphNodes.Length; j++)
                {
                    if (original[i].NodesNumber == GraphNodes[j].NodesNumber)
                        break;
                }

                if(j == GraphNodes.Length)
                {
                    remove.Add(original[i]);
                }
            }

            NodesToRemove = remove;
        }

        /// <summary>
        /// Generuoja koordinates viršūnėms
        /// </summary>
        public void GeneratingCoordinates()
        {
            double step = 2 * Math.PI / GraphNodes.Length;
            int R = Program.RofCircle;

            for (int i = 0; i < GraphNodes.Length; i++)
            {
                double x = R * Math.Cos((i - 1) * step) + Program.NodeXPush;
                double y = R * Math.Sin((i - 1) * step) + Program.NodeYPush;
                GraphNodes[i].AddNodeCoordinates(x, y);
            }
        }

        /// <summary>
        /// Pašalina vienodas briaunas (1 -> 2 = 2 -> 1) tinkama briaunų skaičiui nustatyti 
        /// </summary>
        public void RemoveSameEdges()
        {
            for(int i = 0; i < GraphNodes.Length; i++)
            {
                foreach(Edge edge in GraphNodes[i].edges)
                {
                    for (int j = 0; j < GraphNodes.Length; j++)
                    {
                        if (GraphNodes[j].NodesNumber == edge.endNumber)
                            GraphNodes[j].RemoveEdge(edge.startNumber);
                    }
                }
            }
        }

        /// <summary>
        /// Suskaičiuoja briaunų skaičių
        /// </summary>
        /// <returns></returns>
        public int CountConnections()
        {
            int count = 0;
            for(int i = 0; i < GraphNodes.Length; i++)
            {
                count += GraphNodes[i].edges.Count;
            }

            return count;
        }

        public int GetNodeCount()
        {
            return NodeCount;
        }
    }
}
