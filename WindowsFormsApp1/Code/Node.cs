﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Code
{
    public class Node
    {
        public int NodesNumber;
        double x;
        double y;
        public int[] NumbersOfConnectedNodes;
        public int connectingNodeCount;

        public List<Edge> edges;

        public Node(int number)
        {
            edges = new List<Edge>();
            NodesNumber = number;
            NumbersOfConnectedNodes = new int[Program.MaxNodes];
            x = y = 0;
            connectingNodeCount = 0;
        }

        public Node(int number, int[] numbersOfConnectedNodes, double x, double y)
        {
            edges = new List<Edge>();
            NodesNumber = number;
            NumbersOfConnectedNodes = numbersOfConnectedNodes;
            this.x = x;
            this.y = y;
            connectingNodeCount = numbersOfConnectedNodes.Length;
        }

        public void AddConnectingNodeNumber(int nodeNumber)
        {
            if (nodeNumber > Program.MaxNodes)
                throw new Exception("Node number is too big, graph cannot have that many nodes");

            NumbersOfConnectedNodes[connectingNodeCount++] = nodeNumber;
        }

        public void AddNodeCoordinates(double X, double Y)
        {
            this.x = X;
            this.y = Y;
        }

        public void CheckIfNodesAreConnected(Node nodes)
        {
            for(int i = 0; i < connectingNodeCount; i++)
            {
                if(NumbersOfConnectedNodes[i] == nodes.NodesNumber)
                {
                    Edge edge = new Edge(this.NodesNumber, nodes.NodesNumber, this.x, this.y, nodes.x, nodes.y);
                    edges.Add(edge);
                }
            }
        }

        public void RemoveEdge(int startNodeNumber)
        {
            for(int j = edges.Count - 1; j >= 0; j--)
            {
                if (edges[j].startNumber == NodesNumber && edges[j].endNumber == startNodeNumber)
                    edges.RemoveAt(j);
            }
        }

        public double GetY()
        {
            return y;
        }

        public double GetX()
        {
            return x;
        }

        public int[] GetConnectionArray()
        {
            return NumbersOfConnectedNodes;
        }
    }
}
