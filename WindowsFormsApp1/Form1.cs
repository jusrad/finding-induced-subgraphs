﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Code;

namespace WindowsFormsApp1
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        private Graph mainGraph;    //pagrindinis grafas
        List<Graph> subGraphs;      //pografių sąrašas

        /// <summary>
        /// From konstruktorius inicijuoja vartotojo sąsają ir sukuria pagrindinį grafą pagal tiesiogonių nuorodų masyvą
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            mainGraph = null;
            subGraphs = new List<Graph>();

            // 5 nodes and 6 edges
            int[] L = { 2, 5, 1, 3, 4, 5, 2, 2, 5, 1, 2, 4 };
            int[] lst = { 0, 1, 5, 6, 8, 11 };

            //8 nodes and 13 edges
            //int[] L = { 2, 4, 8, 1, 3, 5, 2, 4, 7, 1, 3, 5, 8, 2, 4, 6, 5, 7, 8, 3, 6, 8, 1, 4, 6, 7 };
            //int[] lst = { 0, 2, 5, 8, 12, 15, 18, 21, 25 };

            Node[] nodes = CreatesNodes(L, lst);
            mainGraph = new Graph(nodes, L.Length / 2);
        }

        /// <summary>
        /// Pirmojo mygtukas nupiešia pagrindinį grafą mažojoje panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {
            MainPanel.Refresh();
            PaintsGraph();
        }

        /// <summary>
        /// Antrasis mygtukas nupiešia pagrindinį grafą, patikrina įvestas k ir m reikšmes ir išveda visus indukuotuosius pografius
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, EventArgs e)
        {
            label3.Text = "";
            flowLayoutPanel1.Controls.Clear();
            subGraphs.Clear();
            PaintsGraph();

            int k = 0;
            int m = 0;

            try
            {
                k = int.Parse(textBox2.Text);
            }
            catch (FormatException)
            {
                throw new FormatException("Įvestas netinkamo formato skaičius k");
            }

            try
            {
                m = int.Parse(textBox1.Text);
            }
            catch (FormatException)
            {
                throw new FormatException("Įvestas netinkamo formato skaičius m");
            }

            if(k > mainGraph.GetNodeCount() || k < 1)
                throw new Exception("Netinkamas pasirinktas viršūnių skaičius");

            GetsSubGraphNodeCombinations(mainGraph.GraphNodes, k);  //generuojame visus derinius ir sudedame į sąrašą

            foreach (Graph graph in subGraphs)
            {
                graph.FindNodesToRemove(mainGraph.GraphNodes);  //randa visas viršūnes, kurias reiks pašalinti
                graph.RemovesNodeFromArray();                   //Pašalinta visas rastas viršūnes
                graph.FindsNodeConnections();                   //Randa viršūnių jungtis pagal vidinį objekto masyvą
                graph.RemoveSameEdges();                        //Pašalina vienodas briaunas (1 -> 2 = 2 -> 1) tinkama briaunų skaičiui nustatyti

                int connections = graph.CountConnections();     //skaičiuojame jungtis

                //Piešiame pografį jeigu briaunų skaičius atitinka įvestą briaunų skaičių
                if(connections == m)
                {
                    Bitmap ngraph = CreatesGraphMap(graph);
                    PictureBox box = new PictureBox
                    {
                        Image = ngraph,
                        Height = 220,
                        Width = 285
                    };
                    flowLayoutPanel1.Controls.Add(box);
                }
            }

            label3.Text = subGraphs.Count + " indukuotuojų pografių";
            label3.Visible = true;
        }

        /// <summary>
        /// Piešia grafa pagrindiniame panel
        /// </summary>
        public void PaintsGraph()
        {
            Graphics formG = MainPanel.CreateGraphics();
            Node[] tempNodeArray = mainGraph.GraphNodes;

            for (int i = 0; i < tempNodeArray.Length; i++)
            {
                foreach(Edge node in tempNodeArray[i].edges)
                {
                    formG.DrawLine(new Pen(Color.Black), (int)node.StartX + Program.LineEndPush, (int)node.StartY + Program.LineEndPush, (int)node.EndX + Program.LineEndPush, (int)node.EndY + Program.LineEndPush);
                }
            }

            for (int i = 0; i < tempNodeArray.Length; i++)
            {
                formG.FillEllipse(new SolidBrush(Color.Red), (int)tempNodeArray[i].GetX(), (int)tempNodeArray[i].GetY(), Program.EllipseSize, Program.EllipseSize);
                formG.DrawString(tempNodeArray[i].NodesNumber.ToString(), new Font("Times New Roman", Program.NumberFontSize), new SolidBrush(Color.Black), (float) tempNodeArray[i].GetX() + Program.NumberPositionPush, (float) tempNodeArray[i].GetY() + Program.NumberPositionPush);
            }
        }

        /// <summary>
        /// Sukuria pografio bitmap paveikslėlį, kuris yra išvedamas į ekraną
        /// </summary>
        /// <param name="graph">Piešiamas grafas</param>
        /// <returns></returns>
        public Bitmap CreatesGraphMap(Graph graph)
        {
            Bitmap temp = new Bitmap(WindowsFormsApp1.Properties.Resources.Untitled);

            using (var graphics = Graphics.FromImage(temp))
            {
                for (int i = 0; i < graph.GraphNodes.Length; i++)
                {
                    foreach (Edge node in graph.GraphNodes[i].edges)
                    {
                        graphics.DrawLine(new Pen(Color.Black), (int)node.StartX + Program.LineEndPush, (int)node.StartY + Program.LineEndPush, (int)node.EndX + Program.LineEndPush, (int)node.EndY + Program.LineEndPush);
                    }
                }

                for (int i = 0; i < graph.GraphNodes.Length; i++)
                {
                    graphics.FillEllipse(new SolidBrush(Color.Red), (int)graph.GraphNodes[i].GetX(), (int)graph.GraphNodes[i].GetY(), Program.EllipseSize, Program.EllipseSize);
                    graphics.DrawString(graph.GraphNodes[i].NodesNumber.ToString(), new Font("Times New Roman", Program.NumberFontSize), new SolidBrush(Color.Black), (float)graph.GraphNodes[i].GetX() + Program.NumberPositionPush, (float)graph.GraphNodes[i].GetY() + Program.NumberPositionPush);
                }
            }

            return temp;
        }

        /// <summary>
        /// Iš tiesioginių nuorodų masyvo sukuria grafo objektą
        /// </summary>
        /// <param name="L">Briaunų masyvas</param>
        /// <param name="lst">Viršūnių adresų masyvas</param>
        /// <returns></returns>
        public Node[] CreatesNodes(int[] L, int[] lst)
        {
            if (lst.Length - 1 > Program.MaxNodes)
                throw new Exception("Per daug viršūnių. Max 10 viršūnių");

            Node[] nodeArray = new Node[lst.Length - 1];
            
            for(int i = 0; i < lst.Length - 1; i++)
            {
                Node temp = new Node(i + 1);

                for(int j = lst[i] + 1; j <= lst[i + 1]; j++)
                {
                    temp.AddConnectingNodeNumber(L[j]);
                }
                nodeArray[i] = temp;
            }

            return nodeArray;
        }

        /// <summary>
        /// Gauna visus viršūnių derinius po k višūnių
        /// </summary>
        /// <param name="nodes">Viršūnių, iš kurių formuosime derinius masyvas</param>
        /// <param name="k">Viršūnių skaičius derinyje</param>
        public void GetsSubGraphNodeCombinations(Node[] nodes, int k)
        {
            int[] temp = new int[k];    // temporary array for combination generation
            Node[] nodeArray = nodes;   // graph node array used to create combinations of nodes

            // initialize first combination
            for (int i = 0; i < k; i++)
            {
                temp[i] = i + 1;
            }

            bool p = temp[0] < nodes.Length - k + 1;    //generation condition, the left element in the last combination always has ant element n-k+1
            while (p)
            {
                AddSubGraphsToList(nodeArray, temp, k);

                int r = k - 1;  //which element of the combination we are changing in the array
                int n = k;      //for checking in the while loop

                while (r >= 0 && temp[r] >= nodes.Length - k + n)
                {
                    r--;        //find which element to increment in the array
                    n--;        //for checking, since array starts at 0
                }

                if(r == -1)
                {
                    p = false;
                }
                else
                {
                    temp[r]++;
                    for (int i = r + 1; i < k; i++)
                        temp[i] = temp[i - 1] + 1;      //increments every element on the right by one
                }
            }
        }

        public void AddSubGraphsToList(Node[] nodes, int[] temp, int k)
        {
            Node[] NewCombination = new Node[k];
            int count = 0;
            for(int i = 0; i < temp.Length; i++)
            {
                for(int j = 0; j < nodes.Length; j++)
                {
                    if(temp[i] == nodes[j].NodesNumber)
                    {
                        NewCombination[count] = new Node(nodes[j].NodesNumber, nodes[j].GetConnectionArray(), nodes[j].GetX(), nodes[j].GetY());
                        count++;
                    }
                }
            }

            Graph subGraph = new Graph(NewCombination);
            subGraphs.Add(subGraph);
        }
    }
}
